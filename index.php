<!-- The first include should be config.php -->
<?php require_once('config.php') ?>
<!-- database connection -->
<?php require_once('D:/xxamp/htdocs/crm_systeem/data_layer/db_connection.inc.php') ?>
<!-- head -->
<?php require_once( ROOT . 'includes/head.inc.php') ?>
	<title>EduChain </title>
</head>
<body>
	<!-- container - wraps whole page -->
	<div class="container">
       
		<!-- navbar -->
		<?php include_once( ROOT . 'includes/navbar.inc.php') ?>
		<!-- // navbar -->		
		
		<!-- Page content -->
		<!-- description -->
		<?php include_once( ROOT . 'includes/beschrijving.inc.php') ?>
		<!-- // description -->

		<!-- overview -->
		<?php include_once( ROOT . 'includes/customer_overview.inc.php') ?>
		<!-- // overview -->


		<!-- // Page content -->		

		<!-- footer -->
		<?php include_once( ROOT . 'includes/footer.inc.php') ?>
        <!-- // footer -->