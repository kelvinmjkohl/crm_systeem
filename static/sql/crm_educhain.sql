-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Gegenereerd op: 03 apr 2019 om 10:49
-- Serverversie: 10.1.37-MariaDB
-- PHP-versie: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crm_educhain`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `contactpersonen`
--

CREATE TABLE `contactpersonen` (
  `contact_id` int(11) NOT NULL,
  `klant_id` int(150) NOT NULL,
  `voornaam` varchar(150) NOT NULL,
  `tussenvoegsel` varchar(150) NOT NULL,
  `achternaam` varchar(150) NOT NULL,
  `email` varchar(150) NOT NULL,
  `telefoon` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `contactpersonen`
--

INSERT INTO `contactpersonen` (`contact_id`, `klant_id`, `voornaam`, `tussenvoegsel`, `achternaam`, `email`, `telefoon`) VALUES
(1, 1, 'Kelvin', '', 'Kohl', 'Kelvin@KelvinResort.nl', 1234567890),
(2, 3, 'Kenny', '', 'Dassen', 'Kenny@DassenBV.com', 987654321),
(3, 4, 'Mick', '', 'Willemsen', 'Mick@MickInc.nl', 1029384756);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `diploma`
--

CREATE TABLE `diploma` (
  `diploma_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `opleiding_id` int(11) NOT NULL,
  `afstudeerjaar` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `klanten`
--

CREATE TABLE `klanten` (
  `klant_id` int(11) NOT NULL,
  `naam` varchar(150) NOT NULL,
  `adres` varchar(150) NOT NULL,
  `postcode` varchar(150) NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `klanten`
--

INSERT INTO `klanten` (`klant_id`, `naam`, `adres`, `postcode`, `status`) VALUES
(1, 'Kelvin\'s Resort', 'Cityflat 1', 'AABB12', 'Actief'),
(3, 'Dassen BV', 'Jan-Peterstraat 1', 'AABB22', 'inactief'),
(4, 'Mick Inc.', 'Kasteelstraat 1', 'AABB12', 'Onderbroken');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `koppeling`
--

CREATE TABLE `koppeling` (
  `koppel_id` int(11) NOT NULL,
  `klant_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `koppeling`
--

INSERT INTO `koppeling` (`koppel_id`, `klant_id`, `product_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 3, 2),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `opdrachten`
--

CREATE TABLE `opdrachten` (
  `opdracht_id` int(11) NOT NULL,
  `klant_id` int(11) NOT NULL,
  `soort` varchar(150) NOT NULL,
  `opmerking` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `opdrachten`
--

INSERT INTO `opdrachten` (`opdracht_id`, `klant_id`, `soort`, `opmerking`, `timestamp`, `status`) VALUES
(1, 1, 'Terugbellen', 'Kelvin\'s Resort heeft vragen over het gekozen pakket. We gaan een contactpersoon hierover contacteren.', '2019-04-02 11:00:41', 'Actief');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `opleiding`
--

CREATE TABLE `opleiding` (
  `opleiding_id` int(11) NOT NULL,
  `naam` int(150) NOT NULL,
  `adres` varchar(150) NOT NULL,
  `postcode` varchar(6) NOT NULL,
  `telefoon` int(10) NOT NULL,
  `email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `producten`
--

CREATE TABLE `producten` (
  `product_id` int(11) NOT NULL,
  `naam` varchar(150) NOT NULL,
  `prijs` int(10) NOT NULL,
  `omschrijving` text NOT NULL,
  `status` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Gegevens worden geëxporteerd voor tabel `producten`
--

INSERT INTO `producten` (`product_id`, `naam`, `prijs`, `omschrijving`, `status`) VALUES
(1, 'Pakket 1', 10, 'Dit is pakket 1', 'Actief'),
(2, 'Pakket 2', 30, 'Dit is pakket 2', 'Actief');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `studenten`
--

CREATE TABLE `studenten` (
  `student_id` int(11) NOT NULL,
  `voornaam` varchar(150) NOT NULL,
  `tussenvoegsel` varchar(150) NOT NULL,
  `achternaam` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `contactpersonen`
--
ALTER TABLE `contactpersonen`
  ADD PRIMARY KEY (`contact_id`);

--
-- Indexen voor tabel `diploma`
--
ALTER TABLE `diploma`
  ADD PRIMARY KEY (`diploma_id`);

--
-- Indexen voor tabel `klanten`
--
ALTER TABLE `klanten`
  ADD PRIMARY KEY (`klant_id`);

--
-- Indexen voor tabel `koppeling`
--
ALTER TABLE `koppeling`
  ADD PRIMARY KEY (`koppel_id`);

--
-- Indexen voor tabel `opdrachten`
--
ALTER TABLE `opdrachten`
  ADD PRIMARY KEY (`opdracht_id`);

--
-- Indexen voor tabel `opleiding`
--
ALTER TABLE `opleiding`
  ADD PRIMARY KEY (`opleiding_id`);

--
-- Indexen voor tabel `producten`
--
ALTER TABLE `producten`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexen voor tabel `studenten`
--
ALTER TABLE `studenten`
  ADD PRIMARY KEY (`student_id`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `contactpersonen`
--
ALTER TABLE `contactpersonen`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `diploma`
--
ALTER TABLE `diploma`
  MODIFY `diploma_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `klanten`
--
ALTER TABLE `klanten`
  MODIFY `klant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `koppeling`
--
ALTER TABLE `koppeling`
  MODIFY `koppel_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `opdrachten`
--
ALTER TABLE `opdrachten`
  MODIFY `opdracht_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT voor een tabel `opleiding`
--
ALTER TABLE `opleiding`
  MODIFY `opleiding_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `producten`
--
ALTER TABLE `producten`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `studenten`
--
ALTER TABLE `studenten`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
