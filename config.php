<?php
	//checks user input and prevents tampering
	if(isset($_POST)) {
		foreach($_POST as $key => $value) {
			$_POST[$key] = strip_tags(trim(addslashes($value)));
		}
	}
	
	if(isset($_GET)) {
		foreach($_GET as $key => $value) {
			$_GET[$key] = strip_tags(trim(addslashes($value)));
		}
	}
	
	error_reporting(E_ALL);
	ini_set("display_errors", "on");

	date_default_timezone_set('Europe/Amsterdam');
	
	define("ROOT"				, "");
	define("INCLUDES_PATH"		, ROOT . "includes/");
	define("IMAGES_PATH"		, ROOT . "images/");
	define("ICONS_PATH"			, IMAGES_PATH . "icons/");
	
	define("DATA_PATH"			, ROOT . "data_layer/");
	define("JS_PATH"			, ROOT . "js/");
	define("CSS_PATH"			, ROOT . "css/");
?>
