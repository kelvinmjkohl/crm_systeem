<?php
// Include config file
require_once "db_connection.inc.php";
 
// Define variables and initialize with empty values
$naam = $adres = $postcode = $status =  "";
$naam_err = $adres_err = $postcode_err = $status_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Validate name
    $input_naam = trim($_POST["naam"]);
    if(empty($input_naam)){
        $naam_err = "Vul alstublieft een naam in.";
    } elseif(!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $naam_err = "Vul alstublieft een valiede naam in.";
    } else{
        $naam = $input_naam;
    }
    
    // Validate adres
    $input_adres = trim($_POST["adres"]);
    if(empty($input_adres)){
        $adres_err = "Vul alstublieft een adres in.";     
    } else{
        $adres = $input_adres;
    }
    
    // Validate postcode
    $input_postcode = trim($_POST["postcode"]);
    if(empty($input_postcode)){
        $postcode_err = "Vul alstublieft een postcode in.";     
    } else{
        $postcode = $input_postcode;
    }
    
     // Validate status
     $input_status = trim($_POST["status"]);
     if(empty($input_status)){
         $status_err = "Vul alstublieft een status in.";     
     } else{
         $status = $input_status;
     }

    // Check input errors before inserting in database
    if(empty($naam_err) && empty($adres_err) && empty($postcode_err) && empty($status_err)){
        // Prepare an insert statement
        $sql = "INSERT INTO klanten (naam, adres, postcode, status) VALUES (?, ?, ?, ?)";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sss", $param_naam, $param_adres, $param_postcode, $param_status);
            
            // Set parameters
            $param_naam = $naam;
            $param_adres = $adres;
            $param_postcode = $postcode;
            $param_status = $status;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records created successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Create Record</h2>
                    </div>
                    <p>Please fill this form and submit to add employee record to the database.</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($naam_err)) ? 'has-error' : ''; ?>">
                            <label>Naam</label>
                            <input type="text" name="naam" class="form-control" value="<?php echo $naam; ?>">
                            <span class="help-block"><?php echo $naam_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($adres_err)) ? 'has-error' : ''; ?>">
                            <label>Adres</label>
                            <textarea name="adres" class="form-control"><?php echo $adres; ?></textarea>
                            <span class="help-block"><?php echo $adres_er;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($postcode_err)) ? 'has-error' : ''; ?>">
                            <label>Postcode</label>
                            <input type="text" name="postcode" class="form-control" value="<?php echo $postcode; ?>">
                            <span class="help-block"><?php echo $postcode_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($status_err)) ? 'has-error' : ''; ?>">
                            <label>Status</label>
                            <input type="text" name="status" class="form-control" value="<?php echo $status; ?>">
                            <span class="help-block"><?php echo $status_err;?></span>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>