<?php
// Include config file
require_once "db_connection.inc.php";
 
// Define variables and initialize with empty values
$naam = $adres = $postcode = $status = "";
$naam_err = $adres_err = $postcode_err = $status_err = "";
 
// Processing form data when form is submitted
if(isset($_POST["id"]) && !empty($_POST["id"])){
    // Get hidden input value
    $id = $_POST["id"];
    
    // Validate naam
    $input_naam = trim($_POST["naam"]);
    if(empty($input_naam)){
        $naam_err = "Please enter a naam.";
    } elseif(!filter_var($input_naam, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+$/")))){
        $naam_err = "Please enter a valid naam.";
    } else{
        $naam = $input_naam;
    }
    
    // Validate adres
    $input_adres = trim($_POST["adres"]);
    if(empty($input_adres)){
        $adres_err = "Please enter an adres.";     
    } else{
        $adres = $input_adres;
    }
    
    // Validate postcode
    $input_postcode = trim($_POST["postcode"]);
    if(empty($input_postcode)){
        $postcode_err = "Please enter the postcode.";     
    } elseif(!ctype_digit($input_postcode)){
        $postcode_err = "Please enter a positive integer value.";
    } else{
        $postcode = $input_postcode;
    }
    
    // Validate status
    $input_status = trim($_POST["status"]);
    if(empty($input_status)){
        $status_err = "Please enter the status.";     
    } elseif(!ctype_digit($input_status)){
        $status_err = "Please enter a positive integer value.";
    } else{
        $status = $input_status;
    }

    // Check input errors before inserting in database
    if(empty($naam_err) && empty($adres_err) && empty($postcode_err)) && empty($status_err)){
        // Prepare an update statement
        $sql = "UPDATE klanten SET name=?, adres=?, postcode=? WHERE klant_id=?";
         
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "sssi", $param_naam, $param_adres, $param_postcode, $param_status, $param_id);
            
            // Set parameters
            $param_naam = $naam;
            $param_adres = $adres;
            $param_postcode = $postcode;
            $param_status = $status;
            $param_id = $klant_id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Records updated successfully. Redirect to landing page
                header("location: index.php");
                exit();
            } else{
                echo "Something went wrong. Please try again later.";
            }
        }
         
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
} else{
    // Check existence of id parameter before processing further
    if(isset($_GET["klant_id"]) && !empty(trim($_GET["klant_id"]))){
        // Get URL parameter
        $id =  trim($_GET["id"]);
        
        // Prepare a select statement
        $sql = "SELECT * FROM klanten WHERE klant_id = ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "i", $param_id);
            
            // Set parameters
            $param_id = $klant_id;
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                $result = mysqli_stmt_get_result($stmt);
    
                if(mysqli_num_rows($result) == 1){
                    /* Fetch result row as an associative array. Since the result set
                    contains only one row, we don't need to use while loop */
                    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
                    
                    // Retrieve individual field value
                    $naam = $row["naam"];
                    $adres = $row["adres"];
                    $postcode = $row["postcode"];
                    $status = $row["status"];
                } else{
                    // URL doesn't contain valid id. Redirect to error page
                    header("location: error.php");
                    exit();
                }
                
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
        
        // Close connection
        mysqli_close($link);
    }  else{
        // URL doesn't contain id parameter. Redirect to error page
        header("location: error.php");
        exit();
    }
}
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Record</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">
    <style type="text/css">
        .wrapper{
            width: 500px;
            margin: 0 auto;
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="page-header">
                        <h2>Update Record</h2>
                    </div>
                    <p>Please edit the input values and submit to update the record.</p>
                    <form action="<?php echo htmlspecialchars(basename($_SERVER['REQUEST_URI'])); ?>" method="post">
                        <div class="form-group <?php echo (!empty($naam_err)) ? 'has-error' : ''; ?>">
                            <label>Naam</label>
                            <input type="text" name="naam" class="form-control" value="<?php echo $naam; ?>">
                            <span class="help-block"><?php echo $naam_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($adres_err)) ? 'has-error' : ''; ?>">
                            <label>Adres</label>
                            <textarea name="adres" class="form-control"><?php echo $adres; ?></textarea>
                            <span class="help-block"><?php echo $adres_err;?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($postcode_err)) ? 'has-error' : ''; ?>">
                            <label>Postcode</label>
                            <input type="text" name="postcode" class="form-control" value="<?php echo $postcode; ?>">
                            <span class="help-block"><?php echo $postcode_err;?></span>
                        </div>
                        <input type="hidden" name="klant_id" value="<?php echo $klant_id; ?>"/>
                        <input type="submit" class="btn btn-primary" value="Submit">
                        <a href="index.php" class="btn btn-default">Cancel</a>
                    </form>
                </div>
            </div>        
        </div>
    </div>
</body>
</html>