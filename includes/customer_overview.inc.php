<div id="customer_overview">
    <?php
        $con = mysqli_connect("$servername","$username","$password","$dbname");
        // Check connection
        if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        
        $result = mysqli_query($con, "SELECT * FROM klanten");
        if (!$result) {
            printf("Error: %s\n", mysqli_error($con));
            exit();
        }
        
        echo "<table border='2'>
            <tr>
            <th>Naam</th>
            <th>Adres</th>
            <th>Postcode</th>
            <th>Status</th>
            <th>View</th>
            <th>Update</th>
            </tr>";
        
            if (mysqli_num_rows($result) > 0) {
                while($row = mysqli_fetch_assoc($result)) {
                    echo "<tr>";
                    echo "<td>" . $row['naam'] . "</td>";
                    echo "<td>" . $row['adres'] . "</td>";
                    echo "<td>" . $row['postcode'] . "</td>";
                    echo "<td>" . $row['status'] . "</td>";
                    echo "<td>" . "<a href='read.php?id=". $row['klant_id'] ."' title='View Record' data-toggle='tooltip'><span class='glyphicon glyphicon-eye-open'></span></a>" . "</td>";
                    echo "<td>" . "<a href='update.php?id=". $row['klant_id'] ."' title='Update Record' data-toggle='tooltip'><span class='glyphicon glyphicon-pencil'></span></a>" . "</td>";
                    // echo "<td>" . "<a href='delete.php?id=". $row['klant_id'] ."' title='Delete Record' data-toggle='tooltip'><span class='glyphicon glyphicon-trash'></span></a>" . "</td>";
                    echo "</tr>";
                }
            }
        echo "</table>";
        
        mysqli_close($con);    
    ?>
</div>